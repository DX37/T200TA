# ASUS T200TA
Useful configs and packages for Arch Linux.

## What's lies in directories
### libs
Library files for ASUS T200TA (can be used for any BayTrail devices with these chips).

`brcmfmac43340-sdio.txt` - Wi-Fi necessary file for `wlan0` device appearing. Got it with command `cat /sys/firmware/efi/efivars/nvram-74b00bd9-805a-4d61-b51f-43268123d113 > /lib/firmware/brcm/brcmfmac43340-sdio.txt`. You should add it to `/lib/firmware/brcm` directory, disable module - `rmmod brcmfmac` - and enable it again - `modprobe brcmfmac`.

`BCM43341B0.hcd` - Bluetooth necessary file for working hci device appearing. Got it from `C:\Windows\System32\drivers` directory as a file named like `BCM43341B0_002.001.014.0122.0176.hcd`. You should add it to `/lib/firmware/brcm` and start `btattach.service`, which is explained below.

### services
It contains only one .service file - `btattach.service`. I got it from install script from [this repo](https://github.com/harryharryharry/x205ta-iso2usb-files). You should add it to `/etc/systemd/system` directory. When you `systemctl start btattach.service`, it activates Bluetooth hci device. Run `systemctl enable btattach.service`, if you need autostart.

### t200ta-pkgs
Packages for ASUS T200TA (can be used for any BayTrail devices with these chips).

This dir contain packages for Arch Linux, built with Wi-Fi and Bluetooth files listed above. This packages installs these files and executing some additional commands, for example, restarting `brcmfmac` module for Wi-Fi work after installing `t200ta-wifi` package or for starting and enabling `btattach.service` after installing `t200ta-bluetooth` package.

Also you can download these packages from AUR. [This is a link to my packages in this repository](https://aur.archlinux.org/packages/?SeB=m&K=DragonX256).
